package com.grailslab

import grails.transaction.Transactional
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsParameterMap

@Transactional
class DeptService {
    def dataSource
    /*static final String[] sortColumns = ['deptNo','deptNo','deptName','deptLoc']
    LinkedHashMap deptPaginateList(GrailsParameterMap params){
        int iDisplayStart = params.iDisplayStart ? params.getInt('iDisplayStart') : CommonUtils.DEFAULT_PAGINATION_START
        int iDisplayLength = params.iDisplayLength ? params.getInt('iDisplayLength') : CommonUtils.DEFAULT_PAGINATION_LENGTH
        String sSortDir = params.sSortDir_0 ? params.sSortDir_0 : CommonUtils.DEFAULT_PAGINATION_SORT_ORDER
        int iSortingCol = params.iSortCol_0 ? params.getInt('iSortCol_0') : CommonUtils.DEFAULT_PAGINATION_SORT_IDX
        //Search string, use or logic to all fields that required to include
        String sSearch = params.sSearch ? params.sSearch : null
        if (sSearch) {
            sSearch = CommonUtils.PERCENTAGE_SIGN + sSearch + CommonUtils.PERCENTAGE_SIGN
        }
        String sortColumn = CommonUtils.getSortColumn(sortColumns,iSortingCol)
        List dataReturns = new ArrayList()
        def c = Dept.createCriteria()
        def results = c.list(max: iDisplayLength, offset: iDisplayStart) {
            *//*and {
                eq("schoolId", schoolService.defaultSchool().id)
                eq("activeStatus", ActiveStatus.ACTIVE)

            }*//*
            if (sSearch) {
                or {
                    ilike('deptName', sSearch)
                    ilike('deptLoc', sSearch)
                }
            }
            order(sortColumn, sSortDir)
        }
        int totalCount = results.totalCount
        int serial = iDisplayStart;
        if (totalCount > 0) {
            if (sSortDir.equals(CommonUtils.SORT_ORDER_DESC)) {
                serial = (totalCount + 1) - iDisplayStart
            }
            results.each { Dept dept ->
                if (sSortDir.equals(CommonUtils.SORT_ORDER_ASC)) {
                    serial++
                } else {
                    serial--
                }
                dataReturns.add([DT_RowId: dept.deptNo, 0: serial, 1: dept.deptNo, 2: dept.deptName, 3:dept.deptLoc, 4: ''])
            }
        }
        return [totalCount:totalCount,results:dataReturns]
    }*/
    def allRowData(){
        Sql sql = new Sql(dataSource)
        List<GroovyRowResult> results = sql.rows("select * from dept")
        return results
    }
    def singleRowData(Integer deptNo, String deptName, String deptLoc){
        Sql sql = new Sql(dataSource)
        List<GroovyRowResult> results = sql.rows("select * from dept where deptno=? and deptname=? and deptloc=?",[deptNo, deptName, deptLoc])
        return results
    }
    def deleteSingleRow(Integer deptNo, String deptName, String deptLoc){
        Sql sql = new Sql(dataSource)
        sql.execute("delete dept where deptno=? and deptname=? and deptloc=?",[deptNo, deptName, deptLoc])
        return true
    }
    def saveDeptInfo(Integer deptNo, String deptName, String deptLoc){
        Sql sql = new Sql(dataSource)
        sql.execute('insert into dept (deptno, deptname, deptloc) values (?,?,?)', [deptNo, deptName, deptLoc])
        return true

    }
    def updateDeptInfo(Integer deptNo, String deptName, String deptLoc, Integer oldDeptNo, String oldDeptname, String oldDeptloc){
        Sql sql = new Sql(dataSource)
        sql.executeUpdate('update dept set deptno=?, deptname=?, deptloc=? where deptno=? and deptname=? and deptloc=?', [deptNo, deptName, deptLoc, oldDeptNo,oldDeptname,oldDeptloc])
        return true

    }

}
