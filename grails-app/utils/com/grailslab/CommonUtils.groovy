package com.grailslab
/**
 * Created by aminul on 12/27/2014.
 */
class CommonUtils {
    public static final String IS_ERROR ="isError"
    public static final String MESSAGE ="message"
    public static final String OBJ ="obj"
    public static final String COMMON_ERROR_MESSAGE ="Something wrong. Please try later or contact admin"
    public static final String COMMON_NOT_FOUND_MESSAGE ="Object not found. Please refresh browser and try again"
    public static final String NOT_APPLICABLE ="N/A"

    //for calender and events
    public static final String CALENDER_EVENT_LIST_INPUT_FORMAT="yyyy-MM-dd"
    public static final String CALENDER_TIME_FORMAT="yyyy-MM-dd'T'HH:mm:ss"

    public static final String UI_DATE_INPUT="dd/MM/yyyy"
    public static final String UI_TIME_INPUT="h:mm a"


    public static final int DEFAULT_PAGINATION_START =0
    public static final int DEFAULT_PAGINATION_LENGTH =25
    public static final int MAX_PAGINATION_LENGTH =100
    public static final String DEFAULT_PAGINATION_SORT_ORDER ='desc'
    public static final String DEFAULT_PAGINATION_SORT_COLUMN ='id'
    public static final int DEFAULT_PAGINATION_SORT_IDX =0
    public static final String PERCENTAGE_SIGN ='%'

    public static final String SORT_ORDER_ASC ='asc'
    public static final String SORT_ORDER_DESC ='desc'
    public static final String UI_DATE_FORMAT ='dd-MMM-yyyy'

    public static String getSortColumn(String [] sortColumns, int idx){
        if(!sortColumns || sortColumns.length<1)
            return DEFAULT_PAGINATION_SORT_COLUMN
        int columnCounts = sortColumns.length
        if(idx>0 && idx<columnCounts){
            return sortColumns[idx]
        }
        return sortColumns[DEFAULT_PAGINATION_SORT_IDX]
    }
    public static String getUiDateStr(Date oldDate){
        if(!oldDate) return ''
        String newDate = oldDate.format(UI_DATE_FORMAT)
        return newDate
    }

    public static String getUiDateStrForCalenderDateEdit(Date oldDate){
        if(!oldDate) return ''
        String newDate = oldDate.format(UI_DATE_INPUT)
        return newDate
    }


}
