package com.grailslab

import grails.converters.JSON

class DeptController {
    def messageSource
    def deptService
    def index() {
        def allDept = deptService.allRowData()
        render(view: 'index', model: [dataReturn: allDept])
    }
    /*def show(){
        def deptValues = Dept.count()
        println("Total Departmanen "+deptValues)
    }*/
    /*def list() {
        LinkedHashMap gridData
        String result
        LinkedHashMap resultMap =deptService.deptPaginateList(params)

        if(!resultMap || resultMap.totalCount== 0){
            gridData = [iTotalRecords: 0, iTotalDisplayRecords: 0, aaData: []]
            result = gridData as JSON
            render result
            return
        }
        int totalCount =resultMap.totalCount
        gridData = [iTotalRecords: totalCount, iTotalDisplayRecords: totalCount, aaData: resultMap.results]
        result = gridData as JSON
        render result
    }*/
    /*def add(){

    }*/
    def edit(DeptCommand command){
        if (!request.method.equals('POST')) {
            redirect(action: 'index')
            return
        }
        LinkedHashMap result = new LinkedHashMap()
        String outPut
        result.put("oldDeptno",command.deptno)
        result.put("oldDeptname",command.deptname)
        result.put("oldDeptloc",command.deptloc)
        def deptno
        String deptname
        String deptloc
        def singleRow = deptService.singleRowData(command.deptno, command.deptname,command.deptloc)
        if(singleRow){
            deptno = singleRow[0].deptno
            deptname = singleRow[0].deptname
            deptloc=singleRow[0].deptloc
        }
        result.put(CommonUtils.IS_ERROR,Boolean.FALSE)
        result.put("deptno",deptno)
        result.put("deptname",deptname)
        result.put("deptloc",deptloc)
        outPut = result as JSON
        render outPut
    }
    def delete(DeptCommand command){
        if (!request.method.equals('POST')) {
            redirect(action: 'index')
            return
        }
        LinkedHashMap result = new LinkedHashMap()
        String outPut
        def singleRow = deptService.deleteSingleRow(command.deptno, command.deptname,command.deptloc)
        result.put(CommonUtils.IS_ERROR,Boolean.FALSE)
        result.put(CommonUtils.MESSAGE,"Deleted Successfully")
        outPut = result as JSON
        render outPut
    }
    def save(DeptCommand command){
        if (!request.method.equals('POST')) {
            redirect(action: 'index')
            return
        }
        LinkedHashMap result = new LinkedHashMap()
        String outPut
        if (command.hasErrors()) {
            def errorList = command?.errors?.allErrors?.collect{messageSource.getMessage(it,null)}
            result.put(CommonUtils.IS_ERROR, Boolean.TRUE)
            result.put(CommonUtils.MESSAGE, errorList?.join('\n'))
            outPut = result as JSON
            render outPut
            return
        }
        String message
        if(command.oldDeptno){
            def saveInfo =deptService.updateDeptInfo(command.deptno,command.deptname,command.deptloc, command.oldDeptno, command.oldDeptname,command.oldDeptloc)
            message = "Department Updated successfully."

        }else {
            def saveInfo =deptService.saveDeptInfo(command.deptno,command.deptname,command.deptloc)
            message = "Department saved successfully."
        }

        result.put(CommonUtils.MESSAGE, message)
        outPut = result as JSON
        render outPut
        return
    }
}
class DeptCommand {
    Integer deptno
    Integer oldDeptno
    String deptname
    String oldDeptname
    String deptloc
    String oldDeptloc

    static constraints = {
        oldDeptno nullable: true
        oldDeptname nullable: true
        oldDeptloc nullable: true
    }
}