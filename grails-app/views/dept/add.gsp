<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		%{--<title>Welcome to Grails ${meta(name: 'app.name')}</title>--}%
		<title>GrailsLab<g:message code="top.header.brand" default="| GrailsLab"/></title>
	</head>
	<body>
    <div class="row">
        <div class="col-md-6" style="padding-top:5px;">
            <!--breadcrumbs start -->
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a class="current" href="${g.createLink(controller: 'dept',action: 'index')}">Home</a>
                        </li>

                    </ul>
            <!--breadcrumbs end -->
        </div>
        <div class="col-md-6">
            <!--breadcrumbs start -->
            <ul class="pull-right" style="padding-top:10px;">
                <button class="btn btn-info btn-add-new-dept" type="button"><i class="fa fa-refresh"></i> Return to List</button>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>
    <div class="row" id="create-from-holder">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Create Dept.
                </header>
                <div class="panel-body">
                    <div class="col-md-8 col-md-offset-1">

                        <form class="cmxform form-horizontal" id="create-form">
                            <g:hiddenField name="oldDeptno"/>
                            <g:hiddenField name="oldDeptname"/>
                            <g:hiddenField name="oldDeptloc"/>

                            <div class="row">
                                <div class="form-group">
                                    <label for="deptno" class="control-label col-md-4">Dept NO</label><span
                                        class="required">*</span>
                                    <div class="col-md-7">
                                        <g:textField class="form-control" id="deptno" tabindex="1" name="deptno"
                                                     placeholder="Enter Department No" required="required"/>
                                    </div>
                                </div>

                            <div class="form-group">
                                <label for="deptname" class="control-label col-md-4">Dept Name</label><span
                                    class="required">*</span>
                                <div class="col-md-7">
                                    <g:textField class="form-control" id="deptname" tabindex="1" name="deptname"
                                                 placeholder="Enter Department Name" required="required"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="deptloc" class="control-label col-md-4">Location</label><span
                                    class="required">*</span>
                                <div class="col-md-7">
                                    <g:textField class="form-control" id="deptloc" tabindex="1" name="deptloc"
                                                 placeholder="Enter Location" required="required"/>
                                </div>
                            </div>
                            </div>

                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-offset-8 col-lg-4">
                                        <button class="btn btn-primary" tabindex="3" type="submit">Save</button>
                                        <button class="btn btn-default cancel-btn" tabindex="4"
                                                type="reset">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </section>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Dept List
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered" id="list-table">
                            <thead>
                            <tr>
                                <th>Dept NO</th>
                                <th>Name</th>
                                <th>Location</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${dataReturn}" var="dataSet">
                                <tr>
                                    <td>${dataSet.deptno}</td>
                                    <td>${dataSet.deptname}</td>
                                    <td>${dataSet.deptloc}</td>
                                    <td>
                                            <span class="col-md-6 no-padding"><a href="" referenceId="${dataSet.deptno}"
                                                                                 class="edit-reference" title="Edit"><span
                                                        class="green glyphicon glyphicon-edit"></span></a></span>
                                            <span class="col-md-6 no-padding"><a href="" referenceId="${dataSet.deptno}"
                                                                                 class="delete-reference"
                                                                                 title="Delete"><span
                                                        class="green glyphicon glyphicon-trash"></span></a></span>
                                    </td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <script>
        jQuery(function ($) {
        var oTable1 = $('#list-table').dataTable();
            $("#create-form").submit(function() {
                jQuery.ajax({
                    type: 'POST',
                    dataType:'JSON',
                    data: $("#create-form").serialize(),
                    url: "${g.createLink(controller: 'dept', action: 'save')}",
                    success: function (data) {
                        if(data.isError==true){
                            alert(data.message);
                        }else {
//                            showSuccessMsg(data.message);
                            window.location.href = "${g.createLink(controller: 'dept',action: 'index')}";
                        }
                    },
                    error: function (XMLHttpRequest, textStatus, errorThrown) {
                    }
                });
                return false; // avoid to execute the actual submit of the form.
            });
            $('.btn-add-new-dept').click(function (e) {
                $("#create-from-holder").toggle(500);
                $("#name").focus();
                e.preventDefault();
            });
        });
    </script>
	</body>
</html>
