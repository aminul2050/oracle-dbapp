<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main"/>
		%{--<title>Welcome to Grails ${meta(name: 'app.name')}</title>--}%
		<title>GrailsLab<g:message code="top.header.brand" default="| GrailsLab"/></title>
	</head>
	<body>
    <div class="row">
        <div class="col-md-6" style="padding-top:5px;">
            <!--breadcrumbs start -->
                    <ul class="breadcrumbs-alt">
                        <li>
                            <a href="#">Home</a>
                        </li>
                        <li>
                            <a  href="#">Pages</a>
                        </li>
                        <li>
                            <a class="current" href="#">Elements</a>
                        </li>
                    </ul>
            <!--breadcrumbs end -->
        </div>
        <div class="col-md-6">
            <!--breadcrumbs start -->
            <ul class="pull-right" style="padding-top:10px;">
                <button class="btn btn-primary" type="button"><i class="fa fa-cloud"></i> Cloud</button>
                <button class="btn btn-success" type="button"><i class="fa fa-eye"></i> View </button>
                <button class="btn btn-info " type="button"><i class="fa fa-refresh"></i> Update</button>
                <button class="btn btn-default " type="button"><i class="fa fa-cloud-upload"></i> Upload</button>
                <button class="btn btn-white" data-toggle="button">
                    <i class="fa fa-thumbs-up "></i>
                    89
                </button>
                <button class="btn btn-white" data-toggle="button">
                    <i class="fa fa-home"></i>
                </button>
            </ul>
            <!--breadcrumbs end -->
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <section class="panel">
                <header class="panel-heading">
                    Heading goes here..
                    <span class="tools pull-right">
                        <a href="javascript:;" class="fa fa-chevron-down"></a>
                        <a href="javascript:;" class="fa fa-cog"></a>
                        <a href="javascript:;" class="fa fa-times"></a>
                    </span>
                </header>
                <div class="panel-body">
                    This is a sample page
                </div>
            </section>
        </div>
    </div>
	</body>
</html>
