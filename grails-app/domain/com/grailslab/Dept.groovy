package com.grailslab

class Dept {
    Integer deptNo
    String deptLoc
    String deptName
    static transients = ['id']
    static mapping = {
        table "dept"
//        id generator: 'assigned', column: "DEPTNO"
        version false
        deptNo column: "deptno"
        deptName column: "deptname"
        deptLoc column: "deptloc"
    }
}
