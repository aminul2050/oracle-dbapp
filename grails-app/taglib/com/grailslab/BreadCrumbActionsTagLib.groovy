package com.grailslab

class BreadCrumbActionsTagLib {
    static defaultEncodeAs = [taglib:'html']
    def breadCrumbActions = {attrs ->
        def breadCrumbText =attrs.breadCrumbText

        boolean SHOW_CREATE_BTN = attrs.SHOW_CREATE_BTN == 'YES';
        def createButtonLevel =attrs.createButtonLevel?:'Add New'
        def createBtnIcon = attrs.createBtnIcon?:"fa fa-plus"
        def createBtnUrl = attrs.createBtnUrl?:'#'

        boolean SHOW_PRINT_BTN = attrs.SHOW_PRINT_BTN == 'YES';
        def printBtnLabel =attrs.printBtnLabel?:'Print'
        def printBtnIcon = attrs.printBtnIcon?:"fa fa-print"
        def printBtnUrl = attrs.printBtnUrl?:'#'

        boolean SHOW_BACK_BTN = attrs.SHOW_BACK_BTN == 'YES';
        def backBtnLabel =attrs.backBtnLabel?:'Back'
        def backBtnIcon = attrs.backBtnIcon?:"glyphicon glyphicon-arrow-left"
        def backBtnUrl = attrs.backBtnUrl?:'#'

        boolean SHOW_LINK_BTN = attrs.SHOW_LINK_BTN == 'YES';
        def linkBtnLabel =attrs.linkBtnLabel?:'Go To'
        def linkBtnIcon = attrs.linkBtnIcon?:"fa fa-external-link"
        def linkBtnUrl = attrs.linkBtnUrl?:'#'

        boolean SHOW_LIST_BTN = attrs.SHOW_LIST_BTN == 'YES';
        def listBtnLabel =attrs.listBtnLabel?:'List'
        def listBtnIcon = attrs.listBtnIcon?:"fa fa-plus"
        def listBtnUrl = attrs.listBtnUrl?:'#'

        boolean SHOW_EXTRA_BTN1 = attrs.SHOW_EXTRA_BTN1 == 'YES';
        def extraBtn1Label =attrs.extraBtn1Label?:'Btn 1'
        def extraBtn1Icon = attrs.extraBtn1Icon?:"fa fa-plus"
        def extraBtn1Url = attrs.extraBtn1Url?:'#'

        boolean SHOW_EXTRA_BTN2 = attrs.SHOW_EXTRA_BTN2 == 'YES';
        def extraBtn2Label =attrs.extraBtn2Label?:'Btn 2'
        def extraBtn2Icon = attrs.extraBtn2Icon?:"fa fa-plus"
        def extraBtn2Url = attrs.extraBtn2Url?:'#'

        out << render(template:"/common/breadCrumbActions", model: [
                breadCrumbText:breadCrumbText,
                SHOW_CREATE_BTN:SHOW_CREATE_BTN,createButtonLevel:createButtonLevel,createBtnIcon:createBtnIcon,createBtnUrl:createBtnUrl,
                SHOW_PRINT_BTN:SHOW_PRINT_BTN, printBtnLabel:printBtnLabel, printBtnIcon:printBtnIcon, printBtnUrl:printBtnUrl,
                SHOW_BACK_BTN:SHOW_BACK_BTN, backBtnLabel:backBtnLabel, backBtnIcon:backBtnIcon, backBtnUrl:backBtnUrl,
                SHOW_LINK_BTN:SHOW_LINK_BTN, linkBtnLabel:linkBtnLabel, linkBtnIcon:linkBtnIcon, linkBtnUrl:linkBtnUrl,
                SHOW_LIST_BTN:SHOW_LIST_BTN, listBtnLabel:listBtnLabel, listBtnIcon:listBtnIcon, listBtnUrl:listBtnUrl,
                SHOW_EXTRA_BTN1:SHOW_EXTRA_BTN1, extraBtn1Label:extraBtn1Label, extraBtn1Icon:extraBtn1Icon, extraBtn1Url:extraBtn1Url,
                SHOW_EXTRA_BTN2:SHOW_EXTRA_BTN2, extraBtn2Label:extraBtn2Label, extraBtn2Icon:extraBtn2Icon, extraBtn2Url:extraBtn2Url
        ])
    }
}
